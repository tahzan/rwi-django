from rwi.core.utils import FilenameGenerator
from django.db import models


class Subscribe(models.Model):
    email = models.CharField(max_length=254, unique=True)
    is_active = models.BooleanField(default=True)
    
    def __str__(self):
        return self.email