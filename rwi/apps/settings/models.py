from rwi.core.utils import FilenameGenerator
from django.db import models


class Settings(models.Model):
    TYPE = (
        ('footer_asia_pacific_office', 'footer_asia_pacific_office'),
        ('footer_adress', 'footer_adress'),
        ('footer_contact', 'footer_contact'),
        ('footer_copyright_text', 'footer_copyright_text'),
        ('footer_desc_left', 'footer_desc_left'),
        ('footer_desc_middle', 'footer_desc_middle'),
        ('footer_desc_right', 'footer_desc_right'),
        ('footer_explor_campaign', 'footer_explor_campaign'),
        ('footer_privacy_policy_url', 'footer_privacy_policy_url'),
        ('footer_link_about', 'footer_link_about'),
        ('footer_web_name', 'footer_web_name'),
        ('join_movement', 'join_movement'),
        ('join_movement_description', 'join_movement_description'),
        ('join_movement_hashtag', 'join_movement_hashtag'),
        ('photos_title', 'photos_title'),
        ('photos_description', 'photos_description'),
        ('subscribe_title', 'subscribe_title'),
        ('subscribe_text', 'subscribe_text'),
        ('top_footer_contact', 'top_footer_contact'),

        ('seo_description', 'seo_description'),
        ('seo_title', 'seo_title'),
        ('seo_keywords', 'seo_keywords'),
        ('seo_image', 'seo_image'),
        ('seo_domain', 'seo_domain'),
        
        ('url_facebook', 'url_facebook'),
        ('url_twitter', 'url_twitter'),
        ('url_instagram', 'url_instagram'),
        ('url_youtube', 'url_youtube'),

        ('videos_highlight_title', 'videos_highlight_title'),
        ('videos_highlight_description', 'videos_highlight_description'),
    )

    name = models.CharField(max_length=254, choices=TYPE, unique=True)
    text_value = models.TextField(blank=True, null=True)
    img_value =  models.ImageField(upload_to=FilenameGenerator(
        prefix='settings'), blank=True, null=True)

    def __str__(self):
        return self.name
