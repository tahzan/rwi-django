from rwi.core.utils import FilenameGenerator, custom_slugify
from django.db import models

class Banner(models.Model):
    class Meta:
        verbose_name = 'Home Banner'
        verbose_name_plural = 'Home Banner'

    img =  models.ImageField(upload_to=FilenameGenerator(
        prefix='banner'))
    big_text = models.CharField(max_length=254, blank=True, null=True)
    small_text = models.CharField(max_length=254, blank=True, null=True)
    video_url = models.CharField(max_length=254, blank=True, null=True)
    find_out_url = models.CharField(max_length=100, blank=True, null=True)
    hastag = models.CharField(max_length=254, blank=True, null=True)
    is_active = models.BooleanField(default=True, blank=True, null=True)
    # base_color = models.CharField("RGBA Color", max_length=20, null=True, help_text='ex: rgba(5,197,214,1)')
    
    def __str__(self):
        return f"{self.id}"
    
class Tag(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

class Videos(models.Model):
    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Video'

    img =  models.ImageField(upload_to=FilenameGenerator(
        prefix='banner'))
    video_url = models.CharField(max_length=254)
    title = models.CharField(max_length=254)
    time = models.CharField(max_length=254)
    slug = models.CharField(unique=True, blank=True, max_length=254)
    tags = models.ManyToManyField(Tag)
    date = models.DateField(auto_now_add=True)
    long_desc = models.TextField("The Story Behind", null=True)
    is_active = models.BooleanField("Published", default=True)
    is_featured = models.BooleanField("Featured", default=True)
    date = models.DateField()

    def __str__(self):
        return self.title

    @property
    def model_name(self):
        return 'video'

    @property
    def short_desc(self):
        return self.long_desc

    def preview(self):
        return mark_safe('<a href="/blog/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.title)
        super(Videos, self).save()



class News(models.Model):
    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'

    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    title = models.CharField(max_length=254)
    date = models.DateField()
    short_desc = models.CharField(max_length=254)
    long_desc = models.TextField()
    is_active = models.BooleanField("Published", default=True)
    is_featured = models.BooleanField("Featured", default=True)
    slug = models.CharField(unique=True, blank=True, max_length=254)
    tags  = models.ManyToManyField(Tag)
    author = models.ForeignKey('user.User', on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        return self.title

    def preview(self):
        return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.title)
        super(News, self).save()
    
    @property
    def model_name(self):
        return 'news'


class Campaign(models.Model):
    class Meta:
        verbose_name = 'Campaign'
        verbose_name_plural = 'Campaign'

    img =  models.ImageField(upload_to=FilenameGenerator(
        prefix='campaign'))
    img2 =  models.ImageField(upload_to=FilenameGenerator(
        prefix='campaign'))
    title = models.CharField(max_length=254)
    hastag = models.CharField(max_length=254)
    short_desc = models.TextField(max_length=254, null=True)
    long_desc = models.TextField(null=True)
    is_featured = models.BooleanField("Featured", default=True)
    video = models.ForeignKey('content.videos', on_delete=models.SET_NULL, null=True)
    base_color = models.CharField("Hex Color", max_length=20)
    slug = models.CharField(unique=True, blank=True, max_length=254, null=True)
    
    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.title)
        super(Campaign, self).save()


class Publication(models.Model):
    class Meta:
        verbose_name = 'Publication'
        verbose_name_plural = 'Publication'

    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='banner'))
    title = models.CharField(max_length=254)
    date = models.DateField()
    short_desc = models.CharField(max_length=254)
    long_desc = models.TextField()
    is_active = models.BooleanField("Published", default=True)
    slug = models.CharField(unique=True, blank=True, max_length=254)
    file =  models.FileField("File", upload_to=FilenameGenerator(
        prefix='publication'), null=True)
    
    def __str__(self):
        return self.title

    def preview(self):
        return mark_safe('<a href="/news/%s" target="_blank"/> preview </a>' % self.slug)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.title)
        super(Publication, self).save()
    
    @property
    def model_name(self):
        return 'publication'



class Album(models.Model):
    class Meta:
        verbose_name = 'Gallery'
        verbose_name_plural = 'Gallery'

    title = models.CharField("Name", max_length=255)
    slug = models.CharField(unique=True, blank=True, max_length=254)
    is_active = models.BooleanField("Published", default=True)
    img =  models.ImageField("Cover Album", upload_to=FilenameGenerator(
        prefix='album'))
    
    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.title)
        super(Album, self).save()


class Photo(models.Model):
    class Meta:
        verbose_name = 'Photo'
        verbose_name_plural = 'Photo'

    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='photo'))
    album = models.ForeignKey('content.Album', on_delete=models.CASCADE)
    is_featured = models.BooleanField("Featured", default=False)
    long_desc = models.CharField("Featured Description", max_length=255, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    
    def __str__(self):
        return f'{self.album.title}-{self.id}' 
