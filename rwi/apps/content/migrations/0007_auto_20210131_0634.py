# Generated by Django 3.1.2 on 2021-01-31 06:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0006_auto_20210131_0425'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='slug',
            field=models.CharField(blank=True, max_length=254, unique=True),
        ),
        migrations.AlterField(
            model_name='videos',
            name='slug',
            field=models.CharField(blank=True, max_length=254, unique=True),
        ),
    ]
