from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import *


class BannerAdmin(SummernoteModelAdmin):    
    summernote_fields = '__all__'

admin.site.register(Banner, BannerAdmin)


class VideosAdmin(SummernoteModelAdmin):    
    summernote_fields = '__all__'

admin.site.register(Videos, VideosAdmin)



admin.site.register(Tag)

class NewsAdmin(SummernoteModelAdmin):    
    summernote_fields = '__all__'
    list_display = ('title', 'slug')

admin.site.register(News, NewsAdmin)


class CampaignAdmin(SummernoteModelAdmin):    
    summernote_fields = '__all__'
    # list_display = ('title', 'preview', 'is_active')

admin.site.register(Campaign, CampaignAdmin)



class PublicationAdmin(SummernoteModelAdmin):    
    summernote_fields = '__all__'
    # list_display = ('title', 'preview', 'is_active')

admin.site.register(Publication, PublicationAdmin)



class AlbumLine(admin.TabularInline):
    model = Photo


class AlbumAdmin(admin.ModelAdmin):
    # list_display = ("name",)
    inlines = (AlbumLine,)

admin.site.register(Album, AlbumAdmin)

admin.site.register(Photo)


from django_summernote.utils import get_attachment_model 
admin.site.unregister(get_attachment_model())
