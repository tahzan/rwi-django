from datetime import timedelta

from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin,
                                        UserManager)
from django.db import models
from django.utils import timezone
from rwi.core.utils import FilenameGenerator

class CustomUserManager(UserManager):
    def create_user(self, username, **extra_fields):
        now = timezone.now()
        user = self.model(username=username,
                          is_superuser=False,
                          last_login=now,
                          **extra_fields)
        
        password = extra_fields.get("password")
        if password:
            user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, password, **extra_fields):
        user = self.create_user(username=username, password=password,
                                **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=255, unique=True, db_index=True)
    email = models.EmailField('email address', max_length=255, null=True,
                              blank=True, db_index=True, default=None)
    first_name = models.CharField(max_length=255, db_index=True)
    last_name = models.CharField(max_length=255)
    about = models.CharField(max_length=255)
    picture = models.ImageField(upload_to=FilenameGenerator(
        prefix='profile_user'), default='', blank=True, null=True)
    is_staff = models.BooleanField('staff status', 
        default=True, blank=True, null=True)
    is_active = models.BooleanField(
        'active', default=True, blank=True, null=True)

    USERNAME_FIELD = 'username'
    objects = CustomUserManager()
    
    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)
    
