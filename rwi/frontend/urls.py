from django.urls import path
from .views import *

app_name = "frontend"
urlpatterns = [
    path('', index, name='index'),
    path("campaigns/", campaign, name="campaign"),
    path("campaign/<str:slug>", campaign_detail, name="campaign_detail"),
    path("news/", news, name="news"),
    path("news/<str:slug>", news_detail, name="news_detail"),
    path("videos/", videos, name="videos"),
    path("videos/<str:slug>", videos_detail, name="videos_detail"),
    path("publications/", publications, name="publications"),
    path("gallery/", gallery, name="gallery"),
    path("gallery/<str:slug>", gallery_detail, name="gallery_detail"),
    path("lazy-load-news/", lazy_load_news, name="lazy_load_news"),
    path("search-publication/", search_publication, name="search_publication"),
    path("search/", search, name="search"),
    path("subscribe/", subscribe, name="subscribe"),
    path('lazy-load-photo/<int:id>', lazy_load_photo, name='lazy_load_photo'),
]

