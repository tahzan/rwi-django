import json
from django.http import JsonResponse
from django.template.response import TemplateResponse
from django.template import loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt

from rwi.apps.content.models import *
from rwi.apps.settings.models import *
from rwi.apps.subscribe.models import *

from rwi.core.serializer import serialize_settings

from django.db.models import Q

# from .forms import ContactusForm


def index(request):
    context = {
        "banners" : Banner.objects.filter(is_active=True).order_by('-id'),
        "latest_videos" : Videos.objects.filter(is_active=True).order_by('-id'),
        "latest_news" : News.objects.filter(is_active=True).order_by('-id').all(),
        "f_video": Videos.objects.filter(is_featured=True).order_by('-id').first(),
        "settings":  get_settings(),
        "featured_photos": Photo.objects.filter(is_featured=True).order_by('-id').all()[:10]
    }
    return TemplateResponse(request, 'frontend/index.html', context)

def campaign(request):
    campaigns = Campaign.objects.all()
    return TemplateResponse(request, 'frontend/campaign.html', {
        "campaigns": campaigns,
        "settings":  get_settings()
    })

def news(request):
    news = News.objects.all()
    featured = news.filter(is_featured=True).order_by('-id').first()
    tag = request.GET.get("tag")
    if tag:
        news = news.filter(tags__in=[tag])


    return TemplateResponse(request, 'frontend/news.html', 
        {
            "newss": news[:4],
            "featured": featured,
            "settings":  get_settings()
        }
    )

def news_detail(request, slug):
    news = News.objects.get(slug=slug)
    taglist = news.tags.all().values_list('id', flat=True)

    newss = (
        News.objects.filter(is_active=True)
        .filter(tags__id__in = taglist)
        .exclude(slug=slug).order_by('-id')
    )

    related_videos = (
        Videos.objects.filter(is_active=True)
        .filter(tags__id__in = taglist)
        .exclude(slug=slug).order_by('-id')
    )

    return TemplateResponse(request, 'frontend/news-detail.html', {
        "news": news, 
        "newss": newss, 
        'related_videos': related_videos,
        "settings":  get_settings(news)
    })


def campaign_detail(request, slug):
    campaign = Campaign.objects.get(slug=slug)
    return TemplateResponse(request, 'frontend/campaign-detail.html', {
        "campaign": campaign, 
    })



def videos(request):
    tag = request.GET.get("tag")
    videos = Videos.objects.all()
    featured = videos.filter(is_featured=True).order_by('-id').first()
    related_videos = (
        Videos.objects.filter(is_active=True)
    )
    if tag:
        videos = videos.filter(tags__in=[tag])
    return TemplateResponse(request, 'frontend/videos.html', 
        {
            "videos": videos,
            "featured": featured,
            "settings":  get_settings(),
            "related_videos": related_videos
        }
    )

def videos_detail(request, slug):
    video = Videos.objects.get(slug=slug)
    taglist = video.tags.all().values_list('id', flat=True)

    related_news = (
        News.objects.filter(is_active=True)
        .filter(tags__id__in = taglist)
        .exclude(slug=slug).order_by('-id')
    )

    related_videos = (
        Videos.objects.filter(is_active=True)
        .filter(tags__id__in = taglist)
        .exclude(slug=slug).order_by('-id')
    )

    return TemplateResponse(request, 'frontend/video-detail.html', {
        "video": video, 
        "newss": related_news, 
        'related_videos': related_videos,
        "settings":  get_settings(video)
    })


def gallery_detail(request, slug):
    page = request.GET.get('page')
    album = Album.objects.get(slug=slug)
    photos = album.photo_set.all()
    results_per_page = 9
    paginator = Paginator(photos, results_per_page)
    try:
        photos = paginator.page(page)
    except PageNotAnInteger:
        photos = paginator.page(1)
    except EmptyPage:
        photos = []

    return TemplateResponse(request, 'frontend/gallery-detail.html', {
        "album": album,
        "photos": photos, 
        "settings":  get_settings()
    })


def publications(request):
    publications = Publication.objects.filter(is_active=True).all()
    
    page = request.GET.get('page')
    results_per_page = 10
    paginator = Paginator(publications, results_per_page)
    try:
        publications = paginator.page(page)
    except PageNotAnInteger:
        publications = paginator.page(1)
    except EmptyPage:
        publications = []

    return TemplateResponse(request, 'frontend/publications.html', 
        {
            "publications": publications,
            "settings":  get_settings()
        }
    )

def search(request):
    query = request.GET.get("query")
    videos = Videos.objects.filter(Q(title__icontains=query)|Q(long_desc__icontains=query)).filter(is_active=True).all()
    newss = News.objects.filter(Q(title__icontains=query)|Q(long_desc__icontains=query)).filter(is_active=True).all()
    publications = Publication.objects.filter(Q(title__icontains=query)|Q(long_desc__icontains=query)).filter(is_active=True).all()
    datas = []
    for video in videos:
        datas.append(video)
    for news in newss:
        datas.append(news)
    for pub in publications:
        datas.append(pub)

    return TemplateResponse(request, 'frontend/search_result.html', 
        {
            "datas": datas,
            "nodata": len(datas) == 0,
            "settings":  get_settings()
        }
    )


def gallery(request):
    albums = Album.objects.filter(is_active=True).all();
    page = request.GET.get('page')
    results_per_page = 9
    paginator = Paginator(albums, results_per_page)
    try:
        albums = paginator.page(page)
    except PageNotAnInteger:
        albums = paginator.page(1)
    except EmptyPage:
        albums = []

    featured = Photo.objects.filter(is_featured=True).order_by('-id').first()
    featured_photos = Photo.objects.filter(is_featured=True).order_by('-id').all()
    return TemplateResponse(request, 'frontend/gallery.html', 
        {
            "albums": albums,
            "featured": featured,
            "featured_photos": featured_photos,
            "settings":  get_settings()
        }
    )

def get_settings(data=None):
    settings = Settings.objects.all()
    serialize_settings_dict={}
    for setting in settings:
        serialize_settings_dict[setting.name] = serialize_settings(setting)
    
    if data is not None:
        serialize_settings_dict['seo_title']['text_value'] = data.title
        serialize_settings_dict['seo_description']['text_value'] = data.short_desc 
        serialize_settings_dict['seo_keywords']['text_value'] = data.short_desc
        serialize_settings_dict['seo_image']['img_value'] = data.img.url

    return serialize_settings_dict


@csrf_exempt
def lazy_load_news(request):
    page = request.POST.get('page')
    news = News.objects.filter(is_active=True).all().order_by('-id')
    results_per_page = 4
    paginator = Paginator(news, results_per_page)
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(2)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    # build a html blogs list with the paginated blogs
    news_html = loader.render_to_string(
        'frontend/news-list.html',
        {'newss': news}
    )
    output_data = {
        'news_html': news_html,
        'has_next': news.has_next()
    }
    return JsonResponse(output_data)


@csrf_exempt
def search_publication(request):
    years = request.POST.get('years')
    years = years.split()
    yearint = []
    for year in years:
        try:
            yearint.append(int(year))
        except:
            pass
    publications = Publication.objects.filter(date__year__in=yearint).order_by('-id')
    
    pubs_html = loader.render_to_string(
        'frontend/pub-list.html',
        {'publications': publications}
    )
    output_data = {
        'pubs_html': pubs_html,
        'count': publications.count(),
    }
    return JsonResponse(output_data)


@csrf_exempt
def subscribe(request):
    email = request.POST.get('email')
    # try :
    Subscribe.objects.create(email=email)
    # except:
    #     pass
    return JsonResponse({"status": "OK"})

@csrf_exempt
def lazy_load_photo(request, id):
    album = Album.objects.filter(id=id).first()
    photos = album.photo_set.all().order_by('-id')
    page = request.POST.get('page')
    results_per_page = 4
    paginator = Paginator(photos, results_per_page)
    has_next = False
    try:
        photos = paginator.page(page)
        has_next = photos.has_next()
    except PageNotAnInteger:
        photos = paginator.page(2)
        has_next = photos.has_next()
    except EmptyPage:
        photos = []

    photos_html = loader.render_to_string(
        'frontend/photos_list.html',
        {'photos': photos}
    )
    output_data = {
        'photos_html': photos_html,
        'has_next': has_next
    }
    return JsonResponse(output_data)