from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

admin.site.site_header = "RWI Admin"
admin.site.site_title = "RWI Admin Portal"
admin.site.index_title = "Welcome to RWI Admin Portal"

urlpatterns = [
    path('rwi-admin/', admin.site.urls),
    path('', include('rwi.frontend.urls', namespace='frontend')),
    path('summernote/', include('django_summernote.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# di server saat settings debug true
if settings.DEBUG:
    urlpatterns + static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)

